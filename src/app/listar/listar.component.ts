import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {

  private url = 'http://localhost:8080/livro';
  excluido = false;
  livro: any;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get(`${this.url}`)
      .subscribe(resp => {
        this.livro = resp;
      });
  }
  deletar(id) {
    this.http.delete(`${this.url}/${id}`)
      .subscribe(
        resultado => {
          alert('Item foi excluido com sucesso');
        },
        erro => {
          if (erro.status === 404) {
            console.log('Produto não localizado.');
          }
        }
      );
  }
}
