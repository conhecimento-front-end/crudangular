import { HttpClient } from '@angular/common/http';
import { async } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-adicionar',
  templateUrl: './adicionar.component.html',
  styleUrls: ['./adicionar.component.css']
})

export class AdicionarComponent implements OnInit {
  // Aqui damos um nome para nosso formulário
  // E ele precisa ser do tipo FormGroup
  livro: FormGroup;
  categoria: FormGroup;
  private url = 'http://localhost:8080/livro';

  // Via DI, nós obtemos o FormBuilder.
  constructor(private http: HttpClient,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    this.criarFormularioDeUsuario();
  }

  criarFormularioDeUsuario() {
    this.livro = this.fb.group({
      id: '',
      nome: '',
      codigo: '',
      paginas: '',
      preco: '',
      categoria: ''
    });
    this.categoria = this.fb.group({
      nome: '',
      id: ''
    });
  }

  enviarDados() {
    this.categoria.value.nome = this.livro.value.categoria;
    console.log('Heheh', this.livro.value);
    this.http.post(`${this.url}`, this.livro.value)
      .subscribe(
        resultado => {
          resultado = this.livro.value.categoria;
          console.log(resultado);
        },
        erro => {
          if (erro.status === 400) {
            console.log(erro);
          }
          else {
            console.log(erro);
          }
        }
      );
  }

}
